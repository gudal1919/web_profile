# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-28 06:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pmsapp', '0002_auto_20180927_1731'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='image',
            field=models.ImageField(default='https://cdn2.iconfinder.com/data/icons/picons-essentials/57/user-512.png', upload_to='media/'),
        ),
    ]
