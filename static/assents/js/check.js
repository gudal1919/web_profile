if(localStorage){
    if(localStorage.getItem("userId")) {
        document.getElementById("login").style.display = "none";
        document.getElementById("logout").style.display = "block";
    } else {
        document.getElementById("login").style.display = "block";
        document.getElementById("logout").style.display = "none";
    }
} else {
    alert("Your browser does not support localStorage, please upgrade or look for different browser");
}
