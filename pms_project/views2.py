from django.http import Http404,HttpResponse
from django.shortcuts import get_object_or_404, render,redirect
from django.utils import timezone
from django import forms
from .models import User
import os
import json
from django.conf import settings
from django.http import JsonResponse

from rest_framework.response import Response

from django.views.decorators.csrf import csrf_protect
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view


def redirect(request):
    context={}
    return render(request, 'redirect.html', context)

def login(request):
    context={}
    if request.method == "POST":
        email=request.POST.__getitem__('email')
        password=request.POST.__getitem__('password')

        user = User.objects.get(email=email,password=password)
        return JsonResponse({"userId": user.id, "message": "Email does not exist in our PMT account"})
    return render(request, 'login.html', context)

@api_view(["POST", "GET"])
def loginfetch(request):
    print(request.method)
    if request.method == "GET":
        template = loader.get_template("loginfetch.html")
        context = {}
        return HttpResponse(template.render(context, request))
    elif request.method == "POST":
        try:
            d=request.data
            print(d)

            email=d['email']
            password=d['password']
            user = User.objects.get(email=email,password=password)
            return JsonResponse(user)
        except :
            return Response({"status": 400, "message": "Email does not exist in our PMT account"}, status=200)
     
    
def register(request):
    context={}
    return render(request, 'register.html', context)

@api_view(["POST", "GET"])    
def user_register(request):
    context={}
    if request.method=="POST":
        fullname=request.POST.__getitem__('fullname')
        email=request.POST.__getitem__('email')
        contact=request.POST.__getitem__('contact')
        address=request.POST.__getitem__('address')
        password=request.POST.__getitem__('password')
        s=User(fullname=fullname,address=address,email=email,contact=contact,password=password) 
        s.save()
        try:
            email=email
            password=password
            user = User.objects.get(email=email,password=password)
            return JsonResponse(user)
            
        except :
            return Response({"status": 400, "message": "Email does not exist in our PMT account"}, status=200)
            
     

def profile(request,id):
    context={}
    print('userid',id)
    user = User.objects.get(pk=id)
    context['user']=user
    return render(request, 'profile.html', context)



def update(request,id):
    id=int(id)
    user = User.objects.get(pk=id)
    context={'user':user}
    return render(request, 'update.html', context)

def updated(request):
    id =request.POST.__getitem__('userId')
    id=int(id)

    user = User.objects.get(pk=id)
    j=request.POST.__getitem__('fullname')
    j1=request.POST.__getitem__('address')
    j2=request.POST.__getitem__('email')
    j3=request.POST.__getitem__('contact')
    j4=request.POST.__getitem__('password')
    
    User.objects.filter(pk=id).update(fullname=j,address=j1,email=j2,contact=j3,password=j4)
    return redirect('/profile/')    

def delete(request,id):
    context={}
    id=int(id)
    user = User.objects.get(pk=id)
    user.active=False
    user.save()
    return redirect('/profile/')    